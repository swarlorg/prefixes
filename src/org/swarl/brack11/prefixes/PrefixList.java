/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.prefixes;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import org.swarl.brack11.prefixes.enums.PrefixKind;
import org.swarl.brack11.prefixes.model.Prefix;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Yury Bondarenko on 2017-03-28.
 * <p>
 * Owner: Yury Bondarenko
 * Website: http://www.swarl.org
 * Email: this.brack@gmail.com
 */
public class PrefixList {
    private static ObservableList<Prefix> records = FXCollections.observableArrayList();

    public PrefixList() {
        try {
            PrefixReader reader = new PrefixReader();
            reader.read();
            records = reader.getRecords();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * gets records of listed kinds
     *
     * @param kinds
     * @return
     */
    public static ObservableList<Prefix> getRecords(PrefixKind[] kinds){
        ObservableList<Prefix> indexRecords = FXCollections.observableArrayList();
        for (Prefix prefix : records) {
            for (PrefixKind kind : kinds) {
                if (prefix.getType().equals(kind)) {
                    indexRecords.add(prefix);
                }
            }
        }
        return indexRecords;
    }

    public ObservableList<Prefix> getRecords(){
        return records;
    }


    public static ObservableList<Prefix> childrenOf(int parentId) {
        if (PrefixList.hasChidren(parentId)) {
            List<Prefix> children = records.stream().filter(prefix ->
                    (prefix.getParent() == parentId) && (PrefixKind.isChildish(prefix.getType()))
            ).collect(Collectors.toList());
            return FXCollections.observableArrayList(children);
        }
        return null;
    }

    public static boolean hasChidren(int parentId) {
        return records.stream().anyMatch(prefix ->
                (prefix.getParent() == parentId) && (PrefixKind.isChildish(prefix.getType()))
        );
    }

    public static Prefix prefixByDxcc(String dxcc) {
        for (Prefix record : records){
            if (dxcc.equalsIgnoreCase(record.getAdif())) {
                return record;
            }
        }
        return null;
    }

    public static Prefix prefixByTerritory(ObservableList<Prefix> prefixes, String territory) {
        try {
            for (Prefix prefix : prefixes) {
                if (territory.equalsIgnoreCase(prefix.getTerritory())) {
                    return prefix;
                }
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Cannot find prefix information for this call \nThrown following exception: " + e.toString());
            alert.showAndWait();
        }
        return null;
    }
}
