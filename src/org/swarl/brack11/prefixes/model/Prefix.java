/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.prefixes.model;


import org.swarl.brack11.prefixes.enums.PrefixKind;

/**
 * Created by Yury Bondarenko on 2017-03-27.
 */

public class Prefix {
    
    private String internalUse  ;
    private double longitude    ;
    private double latitude     ;
    private String territory    ;
    private String prefix       ;
    private String cq           ;
    private String itu          ;
    private String continent    ;
    private String tz           ;
    private String adif         ;
    private String province     ;
    private String startDate    ;
    private String endDate      ;
    private String mask         ;
    private String source       ;

    private int id;
    private int parent          ;
    private int level           ;
    private PrefixKind type     ;


    public Prefix(int id, String internalUse) {
        this.internalUse = internalUse;
        this.id = id;
        this.level = calculateLevel();
        this.type = calculateType();
    }


    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude / 180;
    }

    public void setLongitude(double longitude, boolean ready) {
        if (ready) {
            this.longitude = longitude;
        } else {
            this.longitude = longitude / 180;
        }
    }

    public void setLongitude(String longitude) {
        if (!longitude.equalsIgnoreCase("")) {
            double lon = Double.parseDouble(longitude);
            setLongitude(lon);
        }
    }

    public double getLatitude() {
        return latitude;
    }

    private void setLatitude(double latitude) {
        this.latitude = latitude / 180;
    }

    public void setLatitude(double latitude, boolean ready) {
        if (ready) {
            this.latitude = latitude;
        } else {
            setLatitude(latitude);
        }
    }

    public void setLatitude(String latitude) {
        if (!latitude.equalsIgnoreCase("")) {
            double lat = Double.parseDouble(latitude);
            setLatitude(lat);
        }
    }

    public String getTerritory() {
        return territory;
    }

    public void setTerritory(String territory) {
        this.territory = territory;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getCq() {
        return cq;
    }

    public void setCq(String cq) {
        this.cq = cq;
    }

    public String getItu() {
        return itu;
    }

    public void setItu(String itu) {
        this.itu = itu;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getTz() {
        return tz;
    }

    public void setTz(String tz) {
        this.tz = tz;
    }

    public String getAdif() {
        return adif;
    }

    public void setAdif(String adif) {
        this.adif = adif;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public PrefixKind getType() {
        return type;
    }

    public String toString() {
        String str = territory;
        return str;
    }

    private int calculateLevel() {
        if (internalUse != null) {
            if (internalUse.length() > 2) {
                String s = internalUse.substring(3, internalUse.length());
                return Integer.parseInt(s);
            }
        }
        return 0;
    }

    private PrefixKind calculateType() {
        if (internalUse != null) {
            int typeCode = Integer.parseInt(internalUse.substring(1,2));
            return PrefixKind.getByCode(typeCode);
        }
        return  null;
    }

}
