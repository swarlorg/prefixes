/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.prefixes.model;


import javafx.collections.ObservableList;
import org.swarl.brack11.prefixes.PrefixList;
import org.swarl.brack11.prefixes.enums.DigitalSubdivision;
import org.swarl.brack11.prefixes.enums.OneCharPrefixes;
import org.swarl.brack11.prefixes.enums.PrefixKind;
import org.swarl.brack11.prefixes.enums.RequireSuffix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Call {
    private String call;
    private List<String> part = new ArrayList<>();
    private String prefix;
    private TreeNode<Prefix> prefixTree;
    private String mainCall;
    private boolean isMultipart;
    private String part1;
    private String part2;
    private String part3;

    public Call(String call) {
        this.call = call.replaceAll("\\s+", "").toUpperCase();
        if (isMultipart = this.call.contains("/")) {
            this.part.addAll(Arrays.asList(this.call.split("/")));
        } else {
            this.part.add(this.call);
        }

        prefix = extractPrefix();
        mainCall = extractMainCall();
    }

    public String getPrefix() {
        return prefix;
    }

    public String getMainCall() {
        return mainCall;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public TreeNode<Prefix> prefixTreeOf(PrefixList prefixList) {
        String controll = (isMultipart) ? prefix : call;

        // get top level
        ObservableList<Prefix> dxccList = prefixList.getRecords(new PrefixKind[]{
                PrefixKind.DXCC,
                PrefixKind.NONDXCC
        });
        for (Prefix pfx : dxccList) {
            Pattern mask = Pattern.compile(pfx.getMask());
            Matcher m = mask.matcher(controll);
            if (m.lookingAt()) {
                prefixTree = new TreeNode<Prefix>(pfx);

                // getting level 1
                ObservableList<Prefix> children = PrefixList.childrenOf(pfx.getId());
                if (children != null) {
                    for (Prefix child : children) {
                        Pattern mask1 = Pattern.compile(child.getMask());
                        Matcher m1 = mask1.matcher(controll);
                        if (m1.lookingAt()) {
                             TreeNode<Prefix> level1 = prefixTree.addChild(child);

                            // getting level 2
                             ObservableList<Prefix> subChildren = PrefixList.childrenOf(child.getId());
                             if (subChildren != null) {
                                 for (Prefix subChild : subChildren) {
                                     Pattern mask2 = Pattern.compile(subChild.getMask());
                                     Matcher m2 = mask2.matcher(controll);
                                     if (m2.lookingAt()) {
                                         TreeNode<Prefix> level2 = level1.addChild(subChild);

                                         // getting level 3
                                         ObservableList<Prefix> subSubChildren = PrefixList.childrenOf(subChild.getId());
                                         if (subSubChildren != null) {
                                             for (Prefix subSubChild : subSubChildren) {
                                                 Pattern mask3 = Pattern.compile(subSubChild.getMask());
                                                 Matcher m3 = mask3.matcher(controll);
                                                 if (m3.lookingAt()) {
                                                     TreeNode<Prefix> level3 = level2.addChild(subSubChild);
                                                 }
                                             }
                                         }
                                     }
                                 }
                             }
                        }
                    }
                }
            }
        }

        return prefixTree;
    }


    private String extractPrefix() {
        switch (part.size()) {
            case(2):
                part1 = part.get(0);
                part2 = part.get(1);
                part3 = "";
                break;
            case(3):
                part1 = part.get(0);
                part2 = part.get(1);
                part3 = part.get(2);
                break;
            default:
                part1 = part.get(0);
                part2 = "";
                part3 = "";
        }

        if (RequireSuffix.contains(part1.substring(0,1))) {
            int pos = part1.replaceFirst("^(\\D+).*$", "$1").length();
            return part1.substring(0,pos+2);
        }
        if ((DigitalSubdivision.contains(part1.substring(0,1))) && (isNumber(part2))) {
            if (!isNumber(part1.charAt(1))) {
                return part1.substring(0,2) + part2;
            }
            return part1.substring(0,1) + part2;
        }
        if ((part1.length() > 3) && (isNumber(part1.charAt(3)))) {
            if (isNumber(part1.charAt(2))) {
                return part1.substring(0,3);
            }
            return part1.substring(0,4);
        }
        if ((part1.length() > 2) && (isNumber(part1.charAt(2)))) {
            return part1.substring(0,3);
        }
        if ((part1.length() > 1) && (isNumber(part1.charAt(1))) && (OneCharPrefixes.contains(part1.substring(0,1)))) {
            return part1.substring(0,2);
        }

        return part1.substring(0,3);
    }

    private String extractMainCall() {
        if ((part1.length() > 1) && (part1.length() > part2.length())) {
            return part1;
        }
        return part2;
    }

    private boolean isNumber(String test) {
        return test.matches("[0-9]+");
    }

    private boolean isNumber(char test) {
        return Character.isDigit(test);
    }

}
