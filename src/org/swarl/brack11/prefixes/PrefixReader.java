/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.prefixes;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.swarl.brack11.prefixes.model.Prefix;
import org.swarl.brack11.prefixes.model.TreeNode;

import java.io.*;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by brack on 2017-03-27.
 */
public class PrefixReader {
    private File file;
    private InputStream is;
    private ObservableList<Prefix> records = FXCollections.observableArrayList();
    private ObservableList<TreeNode<Prefix>> treeRecords = FXCollections.observableArrayList();
    private TreeNode<Prefix> pTreeRoot;


    public PrefixReader() {
        //this.file = new File(getClass().getResource("/Prefix.lst").getFile());
        this.is = getClass().getResourceAsStream("/Prefix.lst");

    }


    /*public PrefixReader(File file) {
        this.file = file;
    }*/


    public void read() throws IOException {
        // BufferedReader br = new BufferedReader(new FileReader(file));
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String line = "";
        int lineNr = 0;

        /*switch (prefix.getLevel()) {
            case 0:
                pTreeRoot = new TreeNode<>(prefix);
                break;
            case 1:
                TreeNode<Prefix> level1 = pTreeRoot.addChild(prefix);
                break;
            case 2:
                TreeNode<Prefix> level2 = level1.addChild(prefix);
                break;
            case 3:
                TreeNode<Prefix> level3 = level2.addChild(prefix);
                break;
            case 4:
                TreeNode<Prefix> level4 = level3.addChild(prefix);
                break;
        }*/

        while ((line = br.readLine()) != null) {
            char one = line.charAt(0);

            switch (one) {
                case '#':
                    break;
                default:
                    Prefix prefix = lineToPrefix(lineNr, line);
                    records.add(prefix);
                    parentOf(lineNr++);
            }
        }
    }

    private Prefix lineToPrefix(int lineNr, String line) {

        String[] field = line.split("\\|", -1);

        String internalUse = field[0].replaceAll("[^0-9]", "");

        Prefix prefix = new Prefix(lineNr, internalUse);
        prefix.setLongitude(field[1]);
        prefix.setLatitude(field[2]);
        prefix.setTerritory(field[3]);
        prefix.setPrefix(maskToRegex(field[4]));
        prefix.setCq(field[5]);
        prefix.setItu(field[6]);
        prefix.setContinent(field[7]);
        prefix.setTz(field[8]);
        prefix.setAdif(field[9]);
        prefix.setProvince(field[10]);
        prefix.setMask(maskToRegex(field[13]));

        return prefix;
    }

    public ObservableList<Prefix> getRecords() {
        return records;
    }

    /**
     * Convert mask to java Pattern
     *
     * @param mask
     * @return
     */
    private Pattern maskToPattern(String mask) {

        String regex = maskToRegex(mask);

        Pattern pattern = Pattern.compile(regex);

        return pattern;
    }


    /**
     * Convert mask to java style regex
     *
     * @param mask
     * @return
     */
    private String maskToRegex(String mask) {

        return mask
                .replace("@", "[A-Z]")
                .replace("[/#]", "[/[0-9]]")
                .replace(".", "$")
                .replace("[/?]", "[/A-Z0-9]")
                .replace("#", "[0-9]")
                .replace("?", "[A-Z0-9]")
                .replace(",", "|");
    }


    /**
     * Extracts id of parent-entry
     *
     * @param entryNo id number of the child entry that parent is looked for
     */
    private void parentOf(int entryNo) {
        Prefix currentRecord = records.get(entryNo);
        for (int i = entryNo; i > 0; i--) {
            if (records.get(i).getLevel() < currentRecord.getLevel()) {
                currentRecord.setParent(i);
                if (currentRecord.getLongitude() == 0.0) {
                    currentRecord.setLongitude(records.get(i).getLongitude(), true);
                }
                if (currentRecord.getLatitude() == 0.0) {
                    currentRecord.setLatitude(records.get(i).getLatitude(), true);
                }
                if (currentRecord.getItu().isEmpty()) {
                    currentRecord.setItu(records.get(i).getItu());
                }
                if (currentRecord.getCq().isEmpty()) {
                    currentRecord.setCq(records.get(i).getCq());
                }
                if (currentRecord.getContinent().isEmpty()) {
                    currentRecord.setContinent(records.get(i).getContinent());
                }
                if (currentRecord.getTz().isEmpty()) {
                    currentRecord.setTz(records.get(i).getTz());
                }
                if (currentRecord.getAdif().isEmpty()) {
                    currentRecord.setAdif(records.get(i).getAdif());
                }
                break;
            }
        }
    }

    public ObservableList<Prefix> childrenOf(int parentId) {
        if (hasChidren(parentId)) {
            List<Prefix> children = records.stream().filter(prefix ->
                    prefix.getParent() == parentId
            ).collect(Collectors.toList());
            return FXCollections.observableArrayList(children);
        }
        return null;
    }

    private boolean hasChidren(int parentId) {
        return records.stream().anyMatch(prefix ->
                prefix.getParent() == parentId
        );

    }
}
