/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.prefixes.enums;

public enum PrefixKind {
    NONE(0),
    DXCC(1),
    PROVINCE(2),
    STATION(3),
    DELDXCC(4),
    OLDPREFIX(5),
    NONDXCC(6),
    INVALIDPREFIX(7),
    DELPROVINCE(8),
    CITY(9);

    private int number;

    PrefixKind(int number) {
        this.number = number;
    }

    public static boolean contains(String name){
        for (PrefixKind prefixKind : values()) {
            if (prefixKind.name().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    public static PrefixKind getByCode(int code) {
        for (PrefixKind prefixKind : values()) {
            if (prefixKind.number == code) {
                return prefixKind;
            }
        }
        return null;
    }

    public static boolean isChildish(PrefixKind kind) {
        switch (kind) {
            case STATION:
            case CITY:
            case PROVINCE:
                return true;
            default:
                return false;
        }
    }
}
