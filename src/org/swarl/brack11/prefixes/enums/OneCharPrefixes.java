/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.prefixes.enums;

public enum OneCharPrefixes {
    /**
     * Second line are WPX prefixes
     */
    U, G, F, I, K, N, W,
    R, B, M;

    /**
     * Testing if enum contains tring test
     *
     * @param test
     * @return
     */
    public static boolean contains(String test) {
        for (OneCharPrefixes e : OneCharPrefixes.values()) {
            if (e.name().equals(test)) {
                return true;
            }
        }

        return false;
    }

}
